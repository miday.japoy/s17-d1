
// // const array1 = ['eat', 'sleep'];
// // console.log(array1);

// // const array2 = new Array('pray', 'play');
// // console.log(array1);


// // const myList = [];


// // const numArray = [2, 3, 4, 5];



// // const stringArray = ['eat', 'work', 'pray', 'play'];


// // const newData = ['work', 1, true];


// // const newData1 = [
// // {'task1': 'excercise'},
// // [1,2,3],
// // function hello(){
// // 	console.log('Hi I an array.')
// // }
// // ];

// // console.log(newData1);


// // // Mini Activity


// // const placeToVisit = ['Japan', 'Korea', 'Thailand', 'New York',
// // 'Siargao', 'France', 'Dubai'];

// // console.log(placeToVisit[0]);
// // console.log(placeToVisit);
// // console.log(placeToVisit[placeToVisit.length-1]);
// // console.log(placeToVisit.length);


// // let i;

// // for(let i = 0; i <= placeToVisit.length; i++){
// // 	console.log(placeToVisit[i]);


// // // Array Manipulation

// // let dailyActivities = ['eat', 'work', 'pray', 'play'];
// // dailyActivities.push('excercise');
// // console.log(dailyActivities);

// // dailyActivities.unshift('sleep');
// // console.log(dailyActivities);


// // dailyActivities[2] = 'sing';
// // console.log(dailyActivities);


// // dailyActivities[6] = 'sing';
// // console.log(dailyActivities);


// // //  mini-activity


// // placeToVisit[0] = 'Pasig';
// // placeToVisit[placeToVisit.length-1] = 'San Joaquin HS';
// // console.log(placeToVisit);
// // console.log(placeToVisit[0]);
// // console.log(placeToVisit[placeToVisit.length-1]);


// let array = [];
// console.log(array[0]);
// array[0] = 'Cloud Strife';
// console.log(array);


// // Array Method


// let array12 = ['Juan', 'Pedro', 'Jose', 'Andres']

// array12[array12.length] = 'Francisco';
// console.log(array12);


// // remove last element
// array12.pop();
// console.log(array12);

// // remove first element
// array12.shift();
// console.log(array12)

// // Mini-activity

// array12.shift();
// console.log(array12);

// array12.pop();
// console.log(array12);

// array12.unshift('George');
// console.log(array12);

// array12.push('Michael');
// console.log(array12);



// let numArray = [3, 2, 1, 6, 7, 9];
// numArray.sort((a,b)=> b-a);
// console.log(numArray);


// let arrayStr = ['Marie', 'Zen', 'Jamie', 'Elaine'];
// arrayStr.sort((a,b)=> b-a);
// console.log(arrayStr);

// arrayStr.sort()
// console.log(arrayStr);
// arrayStr.sort().reverse();
// console.log(arrayStr);

// let beatles = ['George', 'John', 'Paul', 'Ringo'];
// let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq']


// lakersPlayers.splice(0, 0, 'Caruso');
// console.log(lakersPlayers);

// lakersPlayers.splice(0, 2);
// console.log(lakersPlayers);


// let computerBrands = ['IBM', 'HP', 'APPLE', 'MSI'];
// // computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer'); 
// // console.log(computerBrands);

// computerBrands.splice(0, 1); 
// console.log(computerBrands);


// // let comp = computerBrands.slice(1, 3); 
// // console.log(comp);


// // mini-activity

// let fonts= ['Times New Roma', 'Comic San Ms', 'Impact', 'Momotype Corsiva', 'Arial', 'Arial Black']


// let videoGame =['PS4', 'PSS', 'Switch', 'Xbox', 'Xbox 1']

// let microsoft = videoGame.slice(videoGame.length-2, videoGame.length);
// console.log(microsoft);

// let nintendo = videoGame.slice(2,4);
// console.log(nintendo);



// let sentence = ['I', 'like', 'JavaScript', '.', 'It', 'is', 'fun', '.'];
// let sentenceString = sentence.toString();
// console.log(sentence);
// console.log(sentenceString);


// let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Army Navy'];
// let sentenceString2 = sentence2.join(" ");
// console.log(sentenceString2)


// mini-activity


			// Given a set of characters, 
			// 	-form the name, "Martin" as a single string.
			// 	-form the name, "Miguel" as a single string.
			// Use the methods we discussed so far.

			// save "Martin" and "Miguel" to variables:
			// 	-name 1 and name 2

			// log both variables on the console.


let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];

let formedName = charArr.slice(5,11);
console.log(formedName);
let newStr =formedName.join("");
console.log(newStr);


let formedName1 = charArr.slice(13,19);
console.log(formedName1);	
let newStr1 =formedName1.join("");
console.log(newStr1);
		



let tasksFriday = ['drink HTML', 'eat JS'];
let tasksSaturday = ['inhale CSS', 'breath BootStrap'];
let tasksSunday = ['Get Git', 'Be Node'];

let weekendTasks = tasksFriday.concat(tasksSaturday, tasksSunday);
console.log(weekendTasks);


let batch131 = ['paolo', 'jamir', 'jed', 'ronel', 'rom', 'jayson'];

console.log(batch131.indexOf('jed'));
console.log(batch131.lastIndexOf('jed'));


/*
	Mini-Activity
	Given a set of brands with some entries repeated:
		Create a function which can display the index of the brand that was input the first time it was found in the array.

		Create a function which can display the index of the brand that was input the last time it was found in the array.
*/
		let carBrands = [
			'BMW',
			'Dodge',
			'Maserati',
			'Porsche',
			'Chevrolet',
			'Ferrari',
			'GMC',
			'Porsche',
			'Mitsubhisi',
			'Toyota',
			'Volkswagen',
			'BMW'
		];

function firstIndex(car) {
	return carBrands.indexOf(car);
	
}

function lastIndex(car) {
	return carBrands.lastIndexOf(car);
}

console.log(firstIndex('Porsche'));
console.log(lastIndex('Porsche'));



// Iterators

let avengers = [
	'hulk',
	'black widow',
	'hawkeye',
	'spider-man',
	'Iron man',
	'Captain America'

];

// forEach() not making new array

avengers.forEach(function(avenger){
	console.log(avenger);
})

console.log(avengers);

let marvelHereos = [
		'Moon Knight',
		'Jessica Jones',
		'Deadpool',
		'Cyclops'
];

marvelHereos.forEach(function(hero){
	// iterate over all the items in Marvel Heroes array and let them join the avengers
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		// Add an if-else wherein Cyclops and Deadpool is not allow to join
		avengers.push(hero);
	}
});
console.log(avengers);



//.map - making new array


let number = [25, 50, 30, 20, 5];


let mappedNumbers = number.map(function(number){
	console.log(number);
	return number * 5
});

console.log(mappedNumbers);


// .every() stop at false


let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18
});

console.log(checkAllAdult);

// some() stop at true

let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score) {
	console.log(score);
	return score >= 80;
})

console.log(checkForPassing)


//filter()


let numbersArray2 = [500, 12, 120 ,60, 30]; 

let divissibleBy5 = numbersArray2.filter(function(number){
	return number % 5 === 0;

})

console.log(divissibleBy5);


// find();

let registeredUsernames = ['pedro101', 'mikeyTheKing', 'superPheonix', 'sheWhoCodes'];


let foundUser = registeredUsernames.find(function(username){
	console.log(username);
	return username === 'sheWhoCodes';
});

console.log((foundUser))

// include()

// .includes() - returns a boolean true if it finds a matching item in the array. 
// Case-sensitive

let registeredEmails = [
	'johnnyPhoenix1991@gmail.com',
	'michealKing@gmail.com',
	'pedro_himself@yahoo.com',
	'sheJonesSmith@gmail.com'
];

let doesEmailExist = registeredEmails.includes('michealKing@gmail.com');
console.log(doesEmailExist);




/*
	Mini-Activity
	Create 2 functions 
		First Function is able to find specified or the username input in our registeredUsernames array.
		Display the result in the console.

		Second Function is able to find a specified email already exist in the registeredEmails array.
			- IF  there is an email found, show an alert:
				"Email Already Exist."
			- IF there is no email found, show an alert:
				"Email is available, proceed to registration."

		You may use any of the three methods we discussed recently.

*/


function usernameFinder(username) {
	let foundUsername = registeredUsernames.includes(username);
	
	if(foundUsername === true){
	console.log('username is valid');} 

	else
	console.log('username is not valid');
		
};
	
console.log(usernameFinder('mikeyTheKing'));


function emailFinder(email) {
	let foundEmail = registeredEmails.includes(email);

	if(foundEmail === true){
	alert("Email Already Exist.");}

	else
	alert("Email is available.");
	

};

console.log(emailFinder('Phoenix1991@gmail.com'));


